<?php
//配置信息
$config = array();
//基本配置
$config['global'] = array(
	'web_path'=>'/'
);
//云平台配置
$config['cloud'] = '';
//平台参数设置
$config['platform'] = array();
//数据库配置
$config['database'] = array(
	'default'=>array(
		'dbname'=>getenv('MYSQL_DBNAME')
		,'drive'=>'mysql'
		,'host'=>getenv('MYSQL_HOST')
		,'port'=>getenv('MYQSL_PORT')
		,'user'=>getenv('MYSQL_USERNAME')
		,'pass'=>getenv('MYSQL_PASSWORD')
	)
);
//模板设置
$config['template'] = array(
	'default'=>array(
		'drive'=>'smarty'
		,'template_dir'=>'$templates/'
	)
);
//cookie
$config['cookie'] = array(
	'key'=>'easymanage'
	,'pass'=>'easymanage'
	,'domain'=>''
	,'path'=>'/'
);
//cache
$config['cache'] = array(
	'default'=>array(
		'drive'=>'memcache'
	)
);
?>